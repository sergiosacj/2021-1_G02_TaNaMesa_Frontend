import simboloCozinha from 'assets/simbolo_cozinha.svg';
import simboloOk from 'assets/simbolo_ok.svg';

const Icons = {
  simbolo_cozinha: simboloCozinha,
  simbolo_ok: simboloOk,
};

export default Icons;
