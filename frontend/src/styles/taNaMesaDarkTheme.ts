export default {
  type: 'dark',
  primary01: '#FF0000',
  primary02: '#5E2129',
  primary03: '#FFA500',
  primary04: '#A45E4D40',

  secondaryYellow: '#E7E400',
  secondaryRed: '#D62631',
  secondaryRed02: '#EB4040',
  secondaryWine: '#9E1D25',
  secondaryGreen: '#2BB426',
  secondaryLightGreen: '#8AED87',

  black: '#FFFFFF',
  white: '#000000',
  gray: '#C4C4C4',
};
