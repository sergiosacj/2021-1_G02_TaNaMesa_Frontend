import imgViewOrders from 'assets/SideBar/visualizar_pedido 1.png';
import imgDoubt from 'assets/SideBar/duvida 1.png';

const homeButtons = [
  {
    id: 0,
    icon: imgViewOrders,
    text: 'Visualizar Pedidos das Mesas',
  },
  {
    id: 1,
    icon: imgDoubt,
    text: 'Mesas que solicitam ajuda',
  },
];

export default homeButtons;
