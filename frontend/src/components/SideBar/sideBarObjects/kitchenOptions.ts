import imgWaiter from 'assets/SideBar/simbolo_usuario 2.png';
import imgOrder from 'assets/SideBar/visualizar_pedido 1.png';

const kitchenOptions = [
  {
    id: 0,
    icon: imgOrder,
    text: 'Visualizar Área de Pedidos',
  },
  {
    id: 1,
    icon: imgWaiter,
    text: 'Chamar Garçom',
  },
];

export default kitchenOptions;
